#  Sprint 1

## 1. Visão Geral


- Data de Início: 01/05/2024;
- Data de Término: 08/05/2024;
- Duração: 7 dias;

## 2. Objetivos
A Sprint 1 tem como objetivo construir o backlog do produto e iniciar o desenvolvimento do MVP, com  as primeiras versões do backend e frontend, além disso serão construidos algumas artefatos referentes à documentação.


##  3. Tarefas da Semana


| Tarefa                           | Responsáveis                          |
|----------------------------------|---------------------------------------|
| Backlog do Produto     | Todos            |
| CRUD de Usuário   (Primeira Versão)               |   |
| &nbsp;&nbsp;&nbsp;&nbsp;Back-End | Lude Ribeiro                        |
| &nbsp;&nbsp;&nbsp;&nbsp;Front-End| Kayro César  e João Vitor            |
| CI/CD / Analytics (Estudo inicial) | Paulo Gonçalves                      |
| Documento de Guia de estilo      | Kayro César, João Vitor               |

- *Obs: No dia 08/05/2024 foi entregue um relatório impresso solicitado pelo professor na semana anterior, no relatório deveria constar a situação atual do projeto, o mesmo foi feito com a contribuição de todos do grupo.





