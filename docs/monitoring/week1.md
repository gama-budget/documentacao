# Semana 1 (03/04 a 10/04)




## **Discussão**


Anteriormente, o grupo fintech era composto por 17 integrantes, mas foi dividido em grupos menores descritos abaixo:

- CryptoBot
- SimulaFin
- SwiftPix
- GammaBudget (Nosso grupo)


<p align="justify"> Após a divisao dos grupos, na primeira semana , os integrantes se concentraram na pesquisa e levantamento de idéias de projeto no contexto de fintechs. Tendo em vista a diversidade de propostas diferentes dessa temática, o professor disponibilizou uma lista de temas a serem escolhidos pelos grupos do tema. Nesse contexto, os integrantes optaram pela escolha do tema relacionado a gestão financeira pessoal, onde existe a preocupação com o controle e gestão de gastos e receitas de uma pessoa no contexto atual. A semana seguinte terá como objetivo a elaboração do Lean Inception. </p>





## **Objetivos**

- Levantar ideias de projeto 
- Escolher o tema do projeto




## **Ações futuras**

- Elaborar o Lean Inception





