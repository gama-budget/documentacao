#  Sprint 8

## 1. Visão Geral


- Data de Início: 19/06/2024;
- Data de Término: 26/06/2024;
- Duração: 7 dias; 

## 2. Objetivos
Por demanda do professor, a Sprint 8 tem como objetivo a participação no Desafio Sebrae, asssitindo cursos, palestras, leitura de materiais, etc. Além disso, o grupo deve elaborar
slides para apresentação do Pitch do nosso produto (o GammaBudget), além da gravação de um video do mesmo para envio no Desafio.


##  3. Tarefas da Semana

| Tarefa                           | Responsáveis                          |
|----------------------------------|---------------------------------------|
| Participar de eventos do Sebrae realizados durante a semana | Todos                       |
| Elaborar slide de apresentação do Pitch |    Todos          |
| Gravar vídeo de apresentação do Pitch  |  Todos 









