#  Sprint 4

## 1. Visão Geral

- Data de Início: 22/05/2024;
- Data de Término: 29/05/2024;
- Duração: 7 dias;

## 2. Objetivos

<p align="justify"> A Sprint 4 tem como objetivo a continuação do desenvolvimento de algumas telas do front-end, de serviços do back-end.  </p>


##  3. Tarefas da Semana

| Tarefa                           | Responsáveis                          |
|----------------------------------|---------------------------------------|
| CRUD de despesas (back-end) | Lude Ribeiro                        |
| Tela de Login (Serviço de integração com o back-end)| João Vitor            |
| Tela de Cadastro de usuário (Serviço de integração com o back-end) | João Vitor             |
| Tela de cadastro de despesas e receitas | Kayro César             |
| Ajustes no Docker do back-end | Paulo Gonçalves                      |
| Criação de testes para o back-end | Paulo Gonçalves                      |




