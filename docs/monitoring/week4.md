# Semana 4 (24/04 a 31/05)


## **Discussão**

<p align="justify"> Durante a semana, nos concentramos na criação do cronograma  de Sprints para orientar o desenvolvimento do projeto. Além disso, configuramos os ambientes de desenvolvimento e iniciamos a construção dos artefatos de documentação. Essas ações foram importantes para alinhar a visão da equipe sobre o futuro e para garantir o desenvolvimento do produto de forma mais organizada. </p>



## **Objetivos**
- Criar cronograma de Sprints 
- Configurar ambientes de desenvolvimento
- Iniciar Contrução de artefatos de documentação

## **Ações futuras**

- Construir Backlog baseado no Lean Inception realizado 




