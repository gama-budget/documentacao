# Plano de comunicação
## Introdução

Durante a elaboração de um plano de comunicação, é crucial considerar diversos aspectos. Entre as responsabilidades envolvidas, incluem-se: a forma como as informações sobre o projeto serão transmitidas e os canais pelos quais serão transmitidas. Além disso, é importante monitorar o melhor horário de disponibilidade para garantir a entrega das demandas existentes. Ao longo do semestre, o grupo realizará várias reuniões para o desenvolvimento do projeto. Com isso em mente, é fundamental mapear os melhores horários para essas reuniões.
## Ferramentas

| Ferramenta | Descrição |
| ---------- | --------- |
| Discord    | Reuniões do time  e pareamentos|
| Whatsapp   | Comunicação rápida entre o time |
| GitLab     | Repositório central para documentação e código |



## Reuniões pré agendadas

- Reunião do grupo: quarta-feira às 20 hrs


## Quadro de Disponibilidade

Para o desenvolvimento do Quadro de Disponibilidade, as informações de disponibilidade de dias e horários de cada integrante foram levantadas. Mais detalhes do quadro estão disponíveis no documento **[Quadro de disponibilidade](../plan/schedule_board.md)**.



## Reunião Geral

A reunião geral é realizada com todos os integrantes do grupo toda quarta-feira às 20h, seguindo a seguinte com  Review e Planning.


Durante o planejamento, ocorre a elaboração do plano de ação para as tarefas que foram identificadas durante a revisão, apresentação das novas tarefas, divisão de atividades e a realização do planning poker para gerar estimativas.

### ATAs de reunião

As ATAs de reunião são resultantes do que foi abordado na reunião, além de ser usada para controle de presença e tempo de duração. 

A ata de reunião aborda os seguintes elementares:

- **Participantes**
- **Duração**
- **Objetivos**
- **Discussão**
- **Ações futuras**


Os detalhamentos de cada ATA nos tópicos mencionados anteriormente estão disponíveis na **[documentação de atas](../meetings/meetingNotes1).**


## Histórico de versões

| Versão | Data       | Descrição | Autores |
| ------ | ---------- | --------- | ------- |
| 1.0    | 07/05/2024 | Criação do documento | Kayro César |