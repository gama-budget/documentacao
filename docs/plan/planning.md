# Metodologias

## Introdução

<p align="justify"> Este documento descreve o processo completo utilizado no desenvolvimento do projeto carteira digital. Para alcançar esse objetivo, adotamos metodologias ágeis de desenvolvimento de software, incluindo Scrum e XP. </p>

## Lean Inception

<p align="justify"> O Lean Inception oferece um processo colaborativo de descoberta e demonstração em que os participantes trabalham juntos por meio de uma série de atividades para entender as oportunidades e elaborar um MVP(mínimo produto viável). O Lean Inception é útil quando a equipe necessita desenvolver um MVP e criar um produto de forma iterativa e incremental . É justamente a metodologia que possibilita acelerar a entrega de soluções de forma contínua e consistente, sempre contemplando dois eixos essenciais: os objetivos do negócio e as necessidades dos usuários. </p>

## Scrum

<p align="justify"> Scrum é um framework de gerenciamento de projetos ágeis, e embora seja amplamente utilizado na área de desenvolvimento de software, pode ser usado para planejar, gerenciar e desenvolver qualquer produto. É um framework interativo e incremental. Nessa metodologia, os projetos são divididos em ciclos curtos e iterativos (repetitivos). Os ciclos duram de 1 a 2 
 semanas e são chamados de sprints . O scrum prevê alguns ritos (cerimônias) e artefatos.
</p>


### <i>Product Backlog</i>

<p align="justify">O <i>Product Backlog</i> é uma lista de tarefas priorizadas que incluem todas as atividades necessárias para concluir um projeto, incluindo requisitos funcionais que agregam valor ao negócio . Ou seja, é um conjunto de funcionalidades e requisitos que deverão ser entregues. </p>

### <i>Sprints</i>

<p align="justify">No Scrum a Sprint é uma iteração que dura de duas a quatro semanas, no qual são desenvolvidos incrementos do produto.</p>

### <i>Planning</i>

<p align="justify">A <i>Sprint Planning</i> é um rito em que a iteração é planejada, em que será definido o que será feito, ou seja, quais itens do backlog serão realizados na <i>sprint </i>.</p>

<p align="justify"> Neste projeto a reunião de planejamento ocorre a cada duas semanas.</p>

### <i>Daily Meeting</i>

<p align="justify"> A <i>Daily</i> é uma reunião diária de acompanhamento da equipe com o objetivo de que cada participante relate seu status e possíveis obstáculos para a conclusão do projeto. Neste projeto a reunião dura em média quinze minutos. </p>

### <i>Sprint Review</i>

<p align="justify"> A <i>Sprint Review</i>  é uma reunião que ocorre no final da <i>sprint</i> em que é feita uma revisão do que foi feito, tendo uma visão geral de como está o produto </p>

### <i>Sprint Retrospective</i>

A <i>Sprint Retrospective</i> é uma reunião realizada no final de uma <i>sprint</i> no contexto da metodologia ágil, como o Scrum. Nesta reunião, a equipe Scrum se reúne para refletir sobre o processo de trabalho durante a <i>sprint</i> recentemente concluída. O principal objetivo da <i>Sprint Retrospective</i> é identificar oportunidades de melhoria, discutir o que funcionou bem e o que pode ser aprimorado no próximo ciclo de desenvolvimento.

## XP

<p align="justify"> O Extreme Programming (XP) é uma metodologia ágil que nasceu com o intuito de tentar solucionar os problemas que eram causados pelas metodologias tradicionais. O XP tem quatro princípios: Comunicação, Simplicidade, Feedback e Coragem.  O XP tem diversos tipos de práticas, totalizando 12 tipos, entre elas a que serão utilizadas no projeto serão: programação em pares, releases curtas, código coletivo e cliente presente.
</p>

### Programação em pares

<p align="justify"> A programação em pares ocorre quando dois programadores escrevem o código juntos em um mesmo computador.
</p>

### Releases curtas

<p align="justify"> São feitas pequenas releases do software, pequenas versões funcionais, para ser entregue ao cliente antes do prazo.
</p>

### Código coletivo

<p align="justify"> Essa característica permite que qualquer programador possa alterar o qualquer código, não precisando de uma autorização de qualquer membro antes disso.
</p>

## Cronograma de Sprints


| Sprint | Número da Semana | Data de Início | Data de Término | Duração (dias) |
|---------------------|------------------|----------------|-----------------|-----------------|
| Sprint 1            | 5                | 01/05/2024     | 08/05/2024      | 7               |
| Sprint 2            | 6                | 08/05/2024     | 15/05/2024      | 7               |
| Sprint 3            | 7                | 15/05/2024     | 22/05/2024      | 7               |
| Sprint 4            | 8               | 22/05/2024     | 29/05/2024      | 7               |
| **Sprint 5 (Entrega 1)**          | **5**             | **29/05/2024**    | **02/06/2024**    |**4**       |
| Sprint 6            | 9               | 05/06/2024     | 12/06/2024      | 7               |
| Sprint 7            | 11               | 12/06/2024     | 19/06/2024      | 7               |
| Sprint 8            | 12               | 19/06/2024     | 26/06/2024      | 7               |
| Sprint 9            | 13               | 26/06/2024     | 03/07/2024      | 7               |
| **Sprint 10  (Entrega 2)**          | **14**               | **03/06/2024**     | **09/07/2024**      | **6**              |

<br>
<br>


<p align="justify">  Obs: As sprints de fato iniciaram nas semana 5, pois nas primeiras semanas da disciplina, os membros do grupo em questão faziam parte de um grande grupo do tema Fintech. Após a separação, o grupo GammaBudget dedicou as primeiras semanas foram destinadas a pesquisa de temas, levantamento de idéias e construção dos artefatos do Lean inception. As sprints iniciaram de fato com o fechamento da ideia do MVP e definição do backlog.

</p>



## Histórico de versões

| Versão | Data       | Descrição             | Autores     |
| ------ | ---------- | --------------------- | ----------- |
| 1.0    | 17/04/2024 | Criação do documento | Kayro César |