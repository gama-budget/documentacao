# Planos Futuros

---

## Ideias que não saíram do papel

Existem alguns planejamentos inicialmente planejados pela equipe, mas que não foram possíveis de serem implementados por falta de tempo ou recursos.

Abaixo, listamos algumas ideias que gostaríamos de ter implementado, mas que não foram possíveis, porém, podem ser implementadas em futuras versões do projeto, por uma equipe que assuma a continuidade do projeto.

-   Integração com uma API como a do [OpenFinance-Brazil](https://openfinancebrasil.org.br) para gerar informações automátizadas sobre receitas e despesas do usuário, eliminando a necessidade de inserção manual de dados.
-   Integração com uma API como a do [Serasa](https://www.serasa.com.br/) para verificar a situação financeira do usuário e sugerir ações para melhorar a situação, como renegociação de dívidas, por exemplo, além de fornecer informações sobre a pontuação de crédito do usuário.
-   Implementação de um app mobile ou PWA para facilitar o acesso dos usuários ao sistema.
-   Implementação de um sistema de notificações para lembrar o usuário de suas contas a pagar e a receber, diretamente no celular, pois a funcionalidade de notificações por email já foi implementada.
-   Implementação de um sistema de metas financeiras mais complexo, onde o usuário pode definir metas de economia e o sistema o ajuda a atingir essas metas, sugerindo ações para economizar dinheiro.
-   Integração com uma API de alguma Inteligência Artificial para fornecer informações mais precisas e personalizadas para o usuário, como sugestões de investimentos, por exemplo.

## Melhorias futuras

Além das ideias que não saíram do papel, existem algumas melhorias que podem ser implementadas em futuras versões do projeto, para melhorar a experiência do usuário e adicionar novas funcionalidades ao sistema.

Abaixo, listamos algumas melhorias que gostaríamos de ter implementado, mas que não foram possíveis, porém, podem ser implementadas em futuras versões do projeto, por uma equipe que assuma a continuidade do projeto.

-   Implementação de um sistema de categorização automática de despesas e receitas, onde o sistema categoriza automaticamente as transações do usuário, eliminando a necessidade de categorização manual.
-   Implementação de um sistema de sugestões de investimentos, onde o sistema sugere investimentos para o usuário com base em seu perfil financeiro e objetivos.
-   Implementação de um sistema de gamificação, onde o usuário ganha pontos e recompensas por atingir metas financeiras e economizar dinheiro.
-   Implementação de um sistema de análise de risco, onde o sistema avalia o risco financeiro do usuário e sugere ações para mitigar esse risco.
-   Implementação de um sistema de análise de crédito, onde o sistema avalia a capacidade de crédito do usuário e sugere ações para melhorar essa capacidade.
-   Implementação de um sistema de análise de investimentos, onde o sistema avalia os investimentos do usuário e sugere ações para melhorar o retorno sobre o investimento.
-   Implementação de um sistema de análise de mercado, onde o sistema avalia o mercado financeiro e sugere ações para aproveitar oportunidades de investimento.

## Histórico de versão

| Data       | Versão | Descrição            | Autor(es)    |
| ---------- | ------ | -------------------- | ------------ |
| 2021-10-10 | 1.0    | Criação do documento | Lude Ribeiro |

---
