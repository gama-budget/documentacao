# Dia 1

## Sobre

No primeiro dia da Lean Inception, são conduzidas três atividades fundamentais para estabelecer uma compreensão alinhada e compartilhada entre a equipe técnica e o cliente em relação ao produto em desenvolvimento. O propósito central dessas etapas é assegurar uma visão clara e precisa do produto e de suas funcionalidades essenciais. Tal alinhamento é crucial para garantir que a equipe técnica compreenda integralmente a natureza do produto e suas metas, garantindo que estas estejam em conformidade com as expectativas e demandas do cliente.

## Visão do Produto

A etapa de "Visão do Produto" em uma Lean Inception é uma atividade crítica que busca estabelecer uma compreensão compartilhada e clara sobre a natureza do produto em desenvolvimento. Durante essa etapa, a equipe de projeto, composta por membros técnicos, especialistas em negócios e representantes do cliente ou usuários finais, reúne-se para criar uma visão abrangente do produto em concepção.

A definição da visão do produto é um passo fundamental para alinhar a equipe em relação aos objetivos e à essência do projeto. Ela serve como um ponto de referência claro para todos os envolvidos, garantindo uma mentalidade e compreensão compartilhadas sobre o que está sendo construído. Esse alinhamento é especialmente vital em projetos ágeis, nos quais a adaptação contínua é valorizada, pois a visão do produto atua como um guia ao longo do desenvolvimento para garantir que o produto permaneça alinhado com os objetivos do cliente e do negócio.

### Visão do Produto 1 do Projeto

![Visão do Produto](../assets/LeanInception/Dia1/Visao/VisaoDeProduto1.png)

### Visão do Produto 2 do Projeto

![Visão do Produto](../assets/LeanInception/Dia1/Visao/VisaoDeProduto2.png)

### Visão do Produto 3 do Projeto

![Visão do Produto](../assets/LeanInception/Dia1/Visao/VisaoDeProduto3.png)

## É/não é - Faz/não faz

A etapa de "É/Não É" e "Faz/Não Faz" é uma atividade destinada a criar listas que estabelecem claramente o que o produto é e o que ele não é, assim como o que ele faz e o que não faz. Essas listas auxiliam na delimitação do escopo do projeto e na prevenção de mal-entendidos entre a equipe técnica e o cliente.


* **É/Não É:**

- "É" refere-se às características, funcionalidades e elementos que definitivamente fazem parte do produto.
- "Não É" refere-se às características, funcionalidades e elementos que definitivamente não fazem parte do produto.

A lista "É/Não É" ajuda a evitar escopo inflado, garantindo que a equipe tenha uma compreensão clara do que deve ser incluído no produto e do que deve ser excluído.

* **Faz/Não Faz:**

- "Faz" refere-se às ações, funcionalidades ou comportamentos específicos que o produto deve realizar com sucesso.
- "Não Faz" refere-se às ações, funcionalidades ou comportamentos que o produto não deve executar ou que não fazem parte de suas responsabilidades.

A lista "Faz/Não Faz" estabelece expectativas claras sobre o comportamento do produto, ajudando a evitar desentendimentos sobre sua funcionalidade.

### É/não é - Faz/não faz 1 do Projeto

![É/não é - Faz/não faz](../assets/LeanInception/Dia1/ENaoEFazNaoFaz/ENaoEFazNaoFaz1.png )

### É/não é - Faz/não faz 2 do Projeto

![É/não é - Faz/não faz](../assets/LeanInception/Dia1/ENaoEFazNaoFaz/ENaoEFazNaoFaz2.png )

## Objetivos do Produto

A etapa de "Objetivos do Produto" em uma Lean Inception é uma atividade crucial que busca estabelecer de forma clara e mensurável quais são os objetivos que o produto deve alcançar. Esses objetivos são fundamentais para guiar o desenvolvimento do produto e assegurar que ele atenda às expectativas do cliente e aos requisitos do negócio.

A partir da definição dos diversos objetivos, são identificados "clusters" que visam agrupá-los em categorias que os generalizem.

### Objetivos do Produto do Projeto

![Objetivos do Produto](../assets/LeanInception/Dia1/ObjetivosDoProduto/ObjetivosDoProduto.png)